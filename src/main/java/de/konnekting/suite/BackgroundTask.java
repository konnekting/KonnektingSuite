/*
 * Copyright (C) 2016 Alexander Christian <alex(at)root1.de>. All rights reserved.
 * 
 * This file is part of KONNEKTING Suite.
 *
 *   KONNEKTING Suite is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   KONNEKTING Suite is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with KONNEKTING DeviceConfig.  If not, see <http://www.gnu.org/licenses/>.
 */
package de.konnekting.suite;

import de.konnekting.suite.events.EventBackgroundThread;
import de.root1.rooteventbus.RootEventBus;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author achristian
 */
public abstract class BackgroundTask implements Runnable {

    private static final Logger log = LoggerFactory.getLogger(BackgroundTask.class);


    private double progress;
    private double step;
    private final String action;
    private static final AtomicInteger COUNTER = new AtomicInteger(0);
    private final int priority;
    
    /**
     * Constructor
     * @param action name of action
     */
    public BackgroundTask(String action) {
        this(action, Thread.NORM_PRIORITY);
    }
    
    /**
     * run the task
     * @param task a backround task to run
     */
    public static void runTask(BackgroundTask task) {
        int i = COUNTER.incrementAndGet();
        long start = System.currentTimeMillis();
        
        RootEventBus.getDefault().post(new EventBackgroundThread(task));
        
        Thread t = new Thread(task, "BackgroundTask(" + i + ") '" + task.getAction() + "'") {

            @Override
            public void run() {
                log.debug("Begin task(" + i + ") '" + task.getAction() + "'");
                try {
                    super.run();
                } finally {
                    log.debug("Finished task(" + i + ") '" + task.getAction() + "' in " + (System.currentTimeMillis() - start) + " ms.");
                }
            }

        };
        t.setPriority(task.getPriority());
        t.start();
    }

    /**
     * Constructor with prio
     * @param action action name
     * @param priority prio
     */
    public BackgroundTask(String action, int priority) {
        this.action = action;
        this.priority = priority;
    }

    @Override
    public abstract void run();

    /**
     * set progress
     * @param progress the progress to set
     */
    public void setProgress(double progress) {
        this.progress = progress;
        RootEventBus.getDefault().post(new EventBackgroundThread(this));
    }

    /**
     * get current progress
     * @return progress in percent
     */
    public double getProgress() {
        return progress;
    }

    /**
     * set how many steps are on the list to do
     * @param steps number of steps
     */
    public void setStepsToDo(int steps) {
        step = 1d / steps;
    }

    /**
     * tell the system that one step is done
     */
    public void stepDone() {
        setProgress(progress + step);
    }

    /**
     * set all done
     */
    public void setDone() {
        progress = 1;
        RootEventBus.getDefault().post(new EventBackgroundThread(this));
    }

    /**
     * check if is done
     * @return true if done
     */
    public boolean isDone() {
        return progress == 1;
    }

    /**
     * get name of action
     * @return action name
     */
    public String getAction() {
        return action;
    }

    /**
     * get priority
     * @return priority
     */
    public int getPriority() {
        return priority;
    }
    
    

}
